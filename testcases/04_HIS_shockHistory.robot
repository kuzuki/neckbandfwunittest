*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    SerialLibrary    encoding=${ENCODING}
Library    String
Library    ../lib/DataLib.py
Suite Setup       Open Serial Port
Suite Teardown    Close Serial Port

*** Variables ***
${SHOCK_COMMAND}    shock-history

*** test cases ***
Validate Response Of Shock History
    [Setup]    Get Test Data    slidingWindow
    ${read_data}    Execute Shock Command And Read Back    ${SHOCK_COMMAND}
    Should Contain    ${read_data}    Time now :
    Should Contain    ${read_data}    OK to shock.
    Should Contain    ${read_data}    ShockHistory Summary: Out Of Range - Shocks enabled