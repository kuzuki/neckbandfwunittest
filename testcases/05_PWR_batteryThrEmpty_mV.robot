*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    SerialLibrary    encoding=${ENCODING}
Library    String
Library    ../lib/DataLib.py
Suite Setup       Open Serial Port
Suite Teardown    Close Serial Port

*** test cases ***
Validate Setting Of batteryThrEmpty_mV
    [Setup]    Get Test Data    batteryThrEmpty_mV
    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    ${PARAM} = ${DEFAULT}