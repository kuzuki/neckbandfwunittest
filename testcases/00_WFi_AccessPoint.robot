*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    SerialLibrary    encoding=${ENCODING}
Library    String
Library    DataLib.py
Suite Setup       Open Serial Port
Suite Teardown    Close Serial Port

*** test cases ***
Validate Default Setting Of AccessPoint
    [Setup]    Get Test Data    AccessPoint
    ${ssid}    ${pw}    Split String    ${DEFAULT}    ,
    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    ${ssid}
    Should Contain    ${read_data}    ${pw}

Validate Set Function Of AccessPoint
    [Setup]    Get Test Data    AccessPoint
    ${ssid}    ${pw}    Split String    ${EXAMPLE}    ,
    ${read_data}     Execute Wfi Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${EXAMPLE}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    ${ssid}
    Should Contain    ${read_data}    ${pw}

Restore Back To Default Value
    [Setup]    Get Test Data    AccessPoint
    ${ssid}    ${pw}    Split String    ${DEFAULT}    ,
    ${read_data}     Execute Wfi Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${DEFAULT}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    ${ssid}
    Should Contain    ${read_data}    ${pw}