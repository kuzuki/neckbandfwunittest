*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    SerialLibrary    encoding=${ENCODING}
Library    String
Library    ../lib/DataLib.py
Suite Setup       Open Serial Port
Suite Teardown    Close Serial Port

*** test cases ***
Validate Default Setting Of LoRa Module bandwidth
    [Setup]    Get Test Data    bandwidth
    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    Bandwidth is ${DEFAULT}

Validate Set Function Of LoRa Module bandwidth
    [Setup]    Get Test Data    bandwidth
    ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${EXAMPLE}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    Bandwidth is ${EXAMPLE}

Validate Min Value Of bandwidth
    [Setup]    Get Test Data    bandwidth
    ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${MIN_VALUE}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    Bandwidth is ${MIN_VALUE}

Validate Max Value Of bandwidth
    [Setup]    Get Test Data    bandwidth
    ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${MAX_VALUE}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    Bandwidth is ${MAX_VALUE}

Restore Back To Default Value
    [Setup]    Get Test Data    bandwidth
    ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${DEFAULT}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    Bandwidth is ${DEFAULT}