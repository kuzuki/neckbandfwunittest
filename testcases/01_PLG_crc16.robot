# *** settings ***
# Resource    ../resources/variables.robot
# Resource    ../resources/keywords.robot
# Library    SerialLibrary    encoding=${ENCODING}
# Library    String
# Library    ../lib/DataLib.py
# Suite Setup       Open Serial Port
# Suite Teardown    Close Serial Port

# *** test cases ***
# Validate Default Setting Of crc16
#     [Setup]    Get Test Data    crc16
#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
#     Should Contain    ${read_data}    ${PARAM} = ${DEFAULT}

# Validate Set Function Of crc16
#     [Setup]    Get Test Data    crc16
#     ${vertices}    ${polygons}    ${crc}    Split String    ${EXAMPLE}    ,

#     #Vertices Set to 5
#     ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    numVertices    ${vertices}
#     Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    numVertices
#     Should Contain    ${read_data}    numVertices = ${vertices}

#     #Polygons Set to 1
#     ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    numPolygons    ${polygons}
#     Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    numPolygons
#     Should Contain    ${read_data}    numPolygons = ${polygons}

#     #crc Set to 39a2
#     ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${crc}
#     Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
#     Should Contain    ${read_data}    crc16 = ${crc}

# Restore Back To Default Value
#     [Setup]    Get Test Data    crc16
#     #Vertices Set to 0
#     ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    numVertices    0
#     Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    numVertices
#     Should Contain    ${read_data}    numVertices = 0

#     #Polygons Set to 0
#     ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    numPolygons    0
#     Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    numPolygons
#     Should Contain    ${read_data}    numPolygons = 0

#     #crc Set to 39a2
#     ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${DEFAULT}
#     Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
#     Should Contain    ${read_data}    crc16 = ${DEFAULT}
