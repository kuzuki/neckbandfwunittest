*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    SerialLibrary    encoding=${ENCODING}
Library    String
Library    ../lib/DataLib.py
Suite Setup       Open Serial Port
Suite Teardown    Close Serial Port

*** test cases ***
# Validate Default Setting Of LoRa Module Frequency
#     [Setup]    Get Test Data    Frequency
#     ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
#     Should Contain    ${read_data}    ${PARAM} is ${DEFAULT} MHz

Validate Set Function Of LoRa Module Frequency
    [Setup]    Get Test Data    Frequency
    ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${EXAMPLE}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    ${PARAM} is ${EXAMPLE} MHz

Restore Back To Default Value
    [Setup]    Get Test Data    Frequency
    ${read_data}    Execute Set Command And Read Back    ${NV_SET}    ${MODULE}    ${PARAM}    ${DEFAULT}
    Should Contain    ${read_data}    ${MSG_SET_SUCEESS}

    ${read_data}    Execute Get Command And Read Back    ${NV_GET}    ${MODULE}    ${PARAM}
    Should Contain    ${read_data}    ${PARAM} is ${DEFAULT} MHz