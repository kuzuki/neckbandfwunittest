import glob
import os
import boto3

class Helper(object):

    fw_name = 'eShepherdCollar_Alpha_1v1.fir'

    def getFirFile(self):
        return glob.glob('fir/*.fir')

    def getFirmwareFirFile(self, sub='fir'):
        return [os.path.basename(x) for x in glob.glob(os.path.join(os.getcwd(), sub, "*.fir" ))]

    def checkFileExist(self, ver, sub='fir'):
        for i in self.getFirmwareFirFile():
            if ver in i:
                return os.path.join(os.getcwd(), sub, i)
        raise FileNotFoundError

    def downloadFWfromS3(self):
        if os.path.exists(self.fw_name):
            os.remove(self.fw_name)
            
        s3 = boto3.client(
            's3',
            aws_access_key_id='AKIASYNJ4IAUNTBEDBGR',
            aws_secret_access_key='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'
        )

        s3.download_file('neckband-firmware', 'firmware/latest/{}'.format(self.fw_name), self.fw_name)

        return glob.glob('*.fir')

    def getFWVerFromFir(self):
        self.downloadFWfromS3()
        
        with open(self.fw_name, 'rb') as fp:
            for line in fp:
                if 'Version' in line.decode('utf-8'):
                    return line.decode('utf-8').split(': ')[1].strip()
