import serial
import sys
import os
import glob
import re
import time
import inspect
import Helper as hp
import CommandSet as cs
from pathlib import Path
from robot.api import logger
from datetime import datetime
from retry import retry

class NeckbandOperation(object):
    
    ROBOT_LIBRARY_SCOPE = 'SUITE'

    def __init__(self, port=None):
        self.speed = 115200
        self.bits = 8
        self.parity = 'N'
        self.stopbits = 1
        self.timeout = 5
        if port is not None:
            self.port = port
        else:
            self.port = self.getCollarPort()
        self.conn = self.openPort()
        logger.info('Create library based on port {}'.format(self.port))

    def __del__(self):
        logger.warn("Destroy library")

    def getAvailablePorts(self):
        port_l = []
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/ttyUSB*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        for port in ports:
            try:
                s = serial.Serial(port, self.speed, self.bits, self.parity, self.stopbits, self.timeout)
                s.close()
                port_l.append(port)
            except (OSError, serial.SerialException):
                pass

        if (len(port_l) != 0):
            return port_l
        else:
            print('Missing ports {}'.format(port_l))
            exit()

    def getCollarPort(self):
        port_list = self.getAvailablePorts()
        for port in port_list:
            with serial.Serial(port, self.speed, self.bits, self.parity, self.stopbits, 30) as conn:
                conn.reset_output_buffer()
                conn.reset_input_buffer()
                # time.sleep(2)
                response = conn.read(200).decode('utf-8', 'ignore')
                logger.console(port)
                logger.console(response)
                if any(re.findall(r'Apptask|gpstask|mpu9250Tas|OTA|nvTask', response, re.IGNORECASE)):
                    conn.close()
                    return port
                conn.close()
        raise ConnectionError
        
    def openPort(self):
        logger.info('Open neckband port = {}'.format(self.port))
        conn = serial.Serial(self.port, self.speed, self.bits, self.parity, self.stopbits, self.timeout)
        return conn

    @retry(ValueError, tries=3, delay=5)
    def writeAndWait(self, command, *term):
        
        if len(term) == 0:
            terminator = 'Ready. (Cmd: `{}`)'.format(command)
        elif len(term) == 1:
            terminator = str(term[0])
        else:
            terminator = list(term)

        self.conn.write('{}\r\n'.format(command).encode())
        self.conn.flush()
        for i in range(200):
            line = self.conn.readline().decode('utf-8', 'ignore')
            logger.info(line)
            if len(term) < 2:
                if terminator in line:
                    logger.info('line #{} - Command: {} -> Respond: {}'.format(i, command, line))
                    # self.conn.close()
                    # time.sleep(1)
                    return line
                else:
                    continue
            else:
                for t in terminator:
                    if t in line:
                        logger.info('line #{} - Command: {} -> Respond: {}'.format(i, command, line))
                        if 'OTA failed to copy image to flash correctly' in line:
                            logger.warn('Found copy image error')
                            raise TimeoutError('OTA failed to copy image to flash correctly')
                        else:
                            return line
                    else:
                        continue
        logger.warn('{}: Response not found.'.format(inspect.stack()[0][3]))
        raise ValueError

    def waitForLine(self, li=200, *term):

        if len(term) == 0:
            terminator = 'Ready. (Cmd: `{}`)'.format(command)
        elif len(term) == 1:
            terminator = str(term[0])
        else:
            terminator = list(term)

        for i in range(int(li)):
            line = self.conn.readline().decode('utf-8', 'ignore')
            if len(term) < 2:
                if terminator in line:
                    logger.info('line #{} - Wait for line -> Respond: {}'.format(i, line))
                    # time.sleep(1)
                    return line
                else:
                    continue
            else:
                for t in terminator:
                    if t in line:
                        logger.info('line #{} - Wait for line -> Respond: {}'.format(i, line))
                        # time.sleep(1)
                        return line
                    else:
                        continue
        return False

    def sendCommand(self, command):
        self.checkPortConnection()
        return self.writeAndWait(command)

    def checkInvalidCommand(self, command):
        self.checkPortConnection()
        return self.writeAndWait(command, 'Invalid variable name')

    def getCommandValue(self, command, parameter):
        # conn = serial.Serial(self.port, self.speed, self.bits, self.parity, self.stopbits, self.timeout)
        self.checkPortConnection()
        if parameter == 'save':
            terminator = 'LRS| Configuration'
        elif parameter == 'magCalibration':
            terminator = 'longtermOffsets='
        elif parameter == 'crc16':
            terminator = 'crc16_calc ='
        elif parameter == 'frequency':
            terminator = 'LoRa Frequency is'
        elif parameter == 'codeRate':
            terminator = 'LoRa Coding Rate is'
        elif parameter == 'spreadFactor':
            terminator = 'LoRa Spread Factor is'
        elif parameter == 'bandwidth':
            terminator = 'LoRa Bandwidth is'
        elif parameter == 'preamble':
            terminator = 'Preamble size of'
        elif parameter == 'timeout':
            terminator = 'Rx time out of'
        elif parameter == 'maxPower':
            terminator = 'LRa| MaxPower'
        elif parameter == 'outputPower':
            terminator = 'LRa| OutputPower'
        elif parameter == 'PaSelect':
            terminator = 'LRa| PaSelect'
        elif parameter == 'txSlot':
            terminator = 'LRS| txSlot is'
        elif parameter == 'txChID':
            terminator = 'LRS| txChID is'
        elif command == 'nv-get SKH':
            terminator = 'OK to shock'
        else:
            terminator = '{} ='.format(parameter)
        for i in range(500):
            self.conn.write('{} {}\r\n'.format(command, parameter).encode())
            self.conn.flush()
            line = self.conn.readline().decode('utf-8', 'ignore')
            if terminator in line:
                logger.info('line #{} - Command: {} -> Respond: {}'.format(i, command, line))
                # conn.close()
                if 'charge_mAh' == parameter:
                    # PWR| charge_mAh = 1961.640 mAh (stored in NV) , 1959.728 mAh (gauge reading)
                    return line.split('=')[1].strip().split(',')[0].strip().split(' ')[0]
                elif parameter in ['save', 'txSlot', 'txChID']:
                    return line
                elif parameter in ['frequency','codeRate','spreadFactor','bandwidth']:
                    return line.split('is')[1].strip()
                elif parameter in ['preamble','timeout']:
                    return line.split('of')[1].strip()
                elif parameter == 'maxPower':
                    return line.split('MaxPower')[1].strip()
                elif parameter == 'outputPower':
                    return line.split('OutputPower')[1].strip()
                elif parameter in ['PaSelect']:
                    return line.split(parameter)[1].strip()[0]
                else:
                    return line.split('=')[1].strip()
            else:
                logger.info('Line {}'.format(i))
                continue
        logger.warn('{}: Response not found.'.format(inspect.stack()[0][3]))

    def checkPortConnection(self):
        for _ in range(3):
            try:
                self.conn.reset_output_buffer()
                self.conn.reset_input_buffer()
                return True
            except ConnectionError:
                time.sleep(5)
                continue
        logger.error('{}: Failed to connect serial port'.format(inspect.stack()[0][3]))
        raise ConnectionError

    @retry((ValueError,TimeoutError), tries=3, delay=5)    
    def upgradeFirmware(self, fw_version=''):
        logger.console('start')

        f = hp.Helper()
        if fw_version:
            fir = f.checkFileExist(fw_version)
        else:
            fir = f.downloadFWfromS3()[0]
            fw_version = f.getFWVerFromFir()
        
        uid = None
        if not self.waitForLine(10, 'Waiting for serial upgrade'):
            uid = self.getDeviceUID()
        else:
            logger.warn('Waiting for serial upgrade found, retry upgrade process')

        self.checkPortConnection()

        logger.info('{}: Sending upgrade command'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        self.writeAndWait('upgrade', 'OTA| Waiting for serial upgrade.')

        logger.info('{}: Sending OTA-start-upgrade command'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        self.writeAndWait('OTA-start-upgrade', 'OTA-packet OK', 'Attempted to begin UART upgrade whilst already upgrading')

        logger.info('{}: Sending fir data'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        with open(fir, 'rb') as fir_file:
            for line in fir_file:
                if (chr(line[0])) != '#':
                    command = bytes('OTA-sendPacket', 'utf-8')
                    space = b' '
                    self.conn.reset_output_buffer()
                    self.conn.reset_input_buffer()
                    self.conn.write(command + space + line)
                    for _ in range(20):
                        response = self.conn.readline().decode('utf-8', 'ignore')
                        if 'OTA-packet OK' in response:
                            time.sleep(0.01)
                            break
                        else:
                            continue
                    # NeckbandCommands.sendCommand(collar.conn, 'OTA-sendPacket' + ' ' + line.decode('utf-8', 'ignore'), 'OTA-packet OK')
                    # raise ValueError('Matching line not found')

        logger.info('{}: Sending OTA-end-upgrade command'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        self.writeAndWait('OTA-end-upgrade', 'Jump To', 'OTA failed to copy image to flash correctly')

        logger.info('{}: Waiting for Complation'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        if not self.waitForLine(200, 'Application started.', 'Patted watchdog'):
            raise ValueError('Matching line not found')
        
        #Wait for NB to be able to response with command like device-info
        time.sleep(2)

        logger.info('{}: Send device-info command'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        app_ver_info = self.writeAndWait('device-info', 'App.Version')

        print('<<<<<<<<<< {} type = {}'.format(fw_version, type(fw_version)))
        print('<<<<<<<<<< {} type = {}'.format(app_ver_info, type(app_ver_info)))
        if fw_version not in app_ver_info:
            raise ValueError('Firmware version not match {} != {}'.format(fw_version, app_ver_info))
        
        if uid:
            logger.info('{}: Validating UID '.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            if not self.setDeviceUID(uid):
                raise ValueError('Failed to set UID')
            else:
                return True

    def getDeviceUID(self):
        raw_uid = self.writeAndWait('nv-uid get', 'NV | UID is')
        c = cs.CommandSet()
        device_uid = c.getUID(raw_uid)

        if not device_uid:
            logger.warn('UID is not set')
            return False
        else:
            return device_uid

    def setDeviceUID(self, uid):
        retry = 3
        while retry:
            logger.info(retry)
            uid = self.getDeviceUID()
            if uid:
                return uid
            else:
                self.writeAndWait('nv-uid set {}'.format(device_uid))
                retry -= 1
        return False

    @retry(ValueError, tries=3, delay=5)
    def getFWVerion(self):
        fw_ver_info = self.writeAndWait('device-info', 'App.Version')
        return fw_ver_info.split(':')[2].strip()

    def readIMUData(self, command):
        self.conn.reset_output_buffer()
        self.conn.reset_input_buffer()
        for i in range(5):
            self.conn.write('{}\r\n'.format(command).encode())
            self.conn.flush()
            for j in range(150):
                line = self.conn.readline().decode('utf-8', 'ignore')
                # print('##### {}'.format(line))
                if any(re.findall(r'[h]:\d+\s[p]:\S+\s[r]:\S+', line, re.IGNORECASE)):
                    # print('Found a match at line #{}: -> {}'.format(i, line))
                    # h:49 p:1 r:-2 s:1 b:0 t:27.7 e:478 d:0 x:372 y:-78 z:-553
                    return [line.strip('\n').split(':')[x].split(' ', 1)[0] for x in range(-11, 0)]
        return [0 for x in range(-11, 0)]

    def waitForIMUtowaitStabilize(self, ti=60):
        time.sleep(ti)
        position = self.readIMUData('imu-derived')
        return position

    def sendRebootCommand(self):
        self.conn.reset_output_buffer()
        self.conn.reset_input_buffer()
        terminator = "Received command: 'reboot'"
        for i in range(200):
            self.conn.write('reboot\r\n'.encode())
            self.conn.flush()
            line = self.conn.readline().decode('utf-8', 'ignore')

            if terminator in line:
                print('Found a match at line #{}: -> {}'.format(i, line))
                time.sleep(1)
                return True
            else:
                continue
        print('Error, response not found.')
