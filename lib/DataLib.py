import pandas as pd

class DataLib():
	def __init__(self):
		self.input_data = './data/NB_CMD.xlsx'

	def getDataByName(self, name):
		workbook = pd.ExcelFile(self.input_data)
		worksheet = workbook.parse('commands')

		for item in worksheet.values.tolist():
			if item[1] == name:
				dataset = item
				break
			else:
				dataset = None
			
		if dataset:
			# map(str, dataset)
			dataset = [str(x).encode('utf-8') for x in dataset]
			return dataset
		else:
			raise ValueError('dataset not found!')

	def intToString(self, data_int):
		return str(data_int).encode('utf-8')