*** settings ***
Library    NeckbandOperation
Library    Helper

*** test cases ***
Upgrade To Latest Firmware Version
    Upgrade Firmware
    ${VER_FIR}    get FWVer From Fir
    Set Suite Metadata    Firmware Version     ${VER_FIR}