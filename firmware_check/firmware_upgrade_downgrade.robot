*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    Helper.py

# Comamnd Example:
# robot -P .\lib\ -v SERVER_COM1:COM3 -v VERSION:4.6.55 firmware_check

*** test cases ***
Upgrade To New Version
    Set Suite Metadata    Firmware Version     ${VERSION}
    Import Library    NeckbandOperation    ${SERVER_COM1}
    Upgrade Firmware    ${VERSION}

Downgrade To Old Version 4.3.1
    Upgrade Firmware    4.3.1
    Upgrade Firmware    ${VERSION}

Downgrade To Old Version 4.4.0
    Upgrade Firmware    4.4.0
    Upgrade Firmware    ${VERSION}

Downgrade To Old Version 4.5.100
    Upgrade Firmware    4.5.100
    Upgrade Firmware    ${VERSION}