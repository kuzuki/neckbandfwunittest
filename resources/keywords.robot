*** Settings ***
Resource    variables.robot

*** Keywords ***
Open Serial Port
    ${PORTS}    List Com Port Names
    Set Global Variable    ${COM}    ${PORTS[0]}
    Add Port    ${COM}
    ...        baudrate=${BAUDRATE}
    ...        bytesize=8
    ...        parity=N
    ...        stopbits=1
    ...        timeout=10

Close Serial Port
    Reset Output Buffer
    Close Port     ${COM}
    Delete Port    ${COM}

Get Test Data
    [Arguments]    ${name}
    ${MODULE}    ${PARAM}    ${DEFAULT}    ${EXAMPLE}    ${MIN_VALUE}    ${MAX_VALUE}    Get Data By Name    ${name}
    Set Global Variable    ${MODULE}
    Set Global Variable    ${PARAM}
    Set Global Variable    ${DEFAULT}
    Set Global Variable    ${EXAMPLE}
    Set Global Variable    ${MIN_VALUE}
    Set Global Variable    ${MAX_VALUE}

Execute Get Command And Read Back
    [Arguments]    ${command}    ${module}    ${param}
    Write Data        ${command} ${module} ${param}${EXECUTE}
    ${data}    Read Until    terminator=${TERMINATOR} (Cmd: `${command} ${module} ${param}`)
    Log To Console    ${data}
    Return From Keyword    ${data}

Execute Wfi Set Command And Read Back
    [Arguments]    ${command}    ${module}    ${param}    ${value}
    Write Data        ${command} ${module} ${param} 0=${value}${EXECUTE}
    ${data}    Read Until    terminator=${TERMINATOR} (Cmd: `${command} ${module} ${param}`)
    Log To Console    ${data}
    Return From Keyword    ${data}

Execute Set Command And Read Back
    [Arguments]    ${command}    ${module}    ${param}    ${value}
    Write Data        ${command} ${module} ${param} ${value}${EXECUTE}
    ${data}    Read Until    terminator=${TERMINATOR}
    Log To Console    ${data}
    Return From Keyword      ${data}

Execute Shock Command And Read Back
    [Arguments]    ${command}
    Write Data        ${command}${EXECUTE}
    ${data}    Read Until    terminator=${TERMINATOR} (Cmd: `${command}`)
    Log To Console    ${data}
    Return From Keyword    ${data}

Setup MetaData
    
    Import Library    NeckbandOperation
    ${ver}    Get FW Verion
    Set Suite Metadata    Firmware Version     ${ver}
    Send Command    KeepAwake on