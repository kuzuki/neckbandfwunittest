*** Variables ***
${VERSION}              4.6.51

${SERVER_COM1}          /dev/ttyUSB0
${SERVER_COM2}          /dev/ttyUSB1
${COM1}                 COM3
${BAUDRATE}             115200
${ENCODING}             ascii

${TERMINATOR}           Ready.
${EXECUTE}              \r\n
${MSG_SET_SUCEESS}      Variable set correctly
${INVALID_VAR_NAME}     Invalid variable name

${NV_GET}               nv-get
${NV_SET}               nv-set

${MODULE}               ${EMPTY}
${PARAM}                ${EMPTY}
${DEFAULT}              ${EMPTY}
${EXAMPLE}              ${EMPTY}
${MIN_VALUE}            ${EMPTY}
${MAX_VALUE}            ${EMPTY}