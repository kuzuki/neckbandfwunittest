*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    NeckbandOperation.py
Suite Setup    Setup MetaData
Test Teardown    Send Command    nv-defaults
Suite Teardown    Send Command    KeepAwake on

*** test cases ***
Send NV default command
    Send Command    nv-defaults

Validate default NV Setting - AppConfig.DoKeepAwake
    ${DoKeepAwake}    Get Command Value    nv-get App    AppConfig.DoKeepAwake
    Log To Console    ${DoKeepAwake}
    Should Be Equal    ${DoKeepAwake}    False
    Send Command    KeepAwake on

Validate default NV Setting - AppConfig.WifiAccessPoint	
    ${WifiAccessPoint}    Get Command Value    nv-get App    AppConfig.WifiAccessPoint
    Log To Console    ${WifiAccessPoint}
    Should Contain    ${WifiAccessPoint}    Not configured

Validate default NV Setting - AppAppConfig.rebootCount
    ${rebootCount}    Get Command Value    nv-get App    AppConfig.rebootCount
    Log To Console    ${rebootCount}
    Should Contain    ${rebootCount}    0

Validate default NV Setting - appNVRamData.loggerMagic
    ${loggerMagic}    Get Command Value    nv-get App    appNVRamData.loggerMagic
    Log To Console    ${loggerMagic}
    Should Be Equal    ${loggerMagic}    0

Validate default NV Setting - IMUlogDisable
    ${IMUlogDisable}    Get Command Value    nv-get App   AppConfig.IMUlogDisable
    Log To Console    ${IMUlogDisable}
    Should Be Equal    ${IMUlogDisable}    True

Validate default NV Setting - AppConfig.RuntimeProfilerEnabled
    ${RuntimeProfilerEnabled}    Get Command Value    nv-get App    AppConfig.RuntimeProfilerEnabled
    Log To Console    ${RuntimeProfilerEnabled}
    Should Be Equal    ${RuntimeProfilerEnabled}    False

Validate default NV Setting - StimulusAlgorithm.NoPulseLimitDistance_m
    ${NoPulseLimitDistance_m}    Get Command Value    nv-get App    StimulusAlgorithm.NoPulseLimitDistance_m
    Log To Console    ${NoPulseLimitDistance_m}
    Should Be Equal    ${NoPulseLimitDistance_m}    0 m

Validate default NV Setting - PWR charge_mAh
    ${charge_mAh}    Get Command Value    nv-get PWR     charge_mAh
    Log To Console    ${charge_mAh}
    Should Be True    ${charge_mAh} > 0
    # PWR| charge_mAh = 1961.640 mAh (stored in NV) , 1959.728 mAh (gauge reading)

# Validate default NV Setting - PLG vertex
#     ${vertex}    Get Command Value    nv-get PLG    vertex
#     Log To Console    ${vertex}
#     Should Be Equal    ${vertex}    0

# Validate default NV Setting - PLG polygon
#     ${polygon}    Get Command Value    nv-get PLG    polygon
#     Log To Console    ${polygon}
#     Should Be Equal    ${polygon}    0

Validate default NV Setting - PLG numVertices
    ${numVertices}    Get Command Value    nv-get PLG    numVertices
    Log To Console    ${numVertices}
    Should Be Equal    ${numVertices}    0

Validate default NV Setting - PLG numPolygons
    ${numPolygons}    Get Command Value    nv-get PLG    numPolygons
    Log To Console    ${numPolygons}
    Should Be Equal    ${numPolygons}    0

Validate default NV Setting - PLG crc16
    ${crc16}    Get Command Value    nv-get PLG    crc16
    Log To Console    ${crc16}
    Should Contain    ${crc16}     0x

Validate default NV Setting - LRa frequency
    ${frequency}    Get Command Value    nv-get LRa    frequency
    Log To Console    ${frequency}
    Should Contain    ${frequency}     923.2

Validate default NV Setting - LRa codeRate
    ${codeRate}    Get Command Value    nv-get LRa    codeRate
    Log To Console    ${codeRate}
    Should Contain    ${codeRate}     4/5

Validate default NV Setting - LRa spreadFactor
    ${spreadFactor}    Get Command Value    nv-get LRa    spreadFactor
    Log To Console    ${spreadFactor}
    Should Contain    ${spreadFactor}     9

Validate default NV Setting - LRa bandwidth
    ${bandwidth}    Get Command Value    nv-get LRa    bandwidth
    Log To Console    ${bandwidth}
    Should Contain    ${bandwidth}     125

Validate default NV Setting - LRa preamble
    ${preamble}    Get Command Value    nv-get LRa    preamble
    Log To Console    ${preamble}
    Should Contain    ${preamble}     20 + 4.25

Validate default NV Setting - LRa timeout
    ${timeout}    Get Command Value    nv-get LRa    timeout
    Log To Console    ${timeout}
    Should Contain    ${timeout}     16

Validate default NV Setting - LRa maxPower
    ${maxPower}    Get Command Value    nv-get LRa    maxPower
    Log To Console    ${maxPower}
    Should Contain    ${maxPower}     0

Validate default NV Setting - LRa outputPower
    ${outputPower}    Get Command Value    nv-get LRa    outputPower
    Log To Console    ${outputPower}
    Should Contain    ${outputPower}     5

Validate default NV Setting - LRa PaSelect
    ${PaSelect}    Get Command Value    nv-get LRa    PaSelect
    Log To Console    ${PaSelect}
    Should Be Equal    ${PaSelect}     1

Validate default NV Setting - LRS timing.t1
    ${t1}    Get Command Value    nv-get LRS    timing.t1
    Log To Console    ${t1}
    Should Contain    ${t1}     0 s

Validate default NV Setting - LRS timing.t2
    ${t2}    Get Command Value    nv-get LRS    timing.t2
    Log To Console    ${t2}
    Should Contain    ${t2}     2250 ms

Validate default NV Setting - LRS timing.t3
    ${t3}    Get Command Value    nv-get LRS    timing.t3
    Log To Console    ${t3}
    Should Contain    ${t3}     750 ms

Validate default NV Setting - LRS timing.t4
    ${t4}    Get Command Value    nv-get LRS    timing.t4
    Log To Console    ${t4}
    Should Contain    ${t4}     0 ms

Validate default NV Setting - LRS timing.t6
    ${t6}    Get Command Value    nv-get LRS    timing.t6
    Log To Console    ${t6}
    Should Contain    ${t6}     2250 ms

Validate default NV Setting - LRS timing.t7
    ${t7}    Get Command Value    nv-get LRS    timing.t7
    Log To Console    ${t7}
    Should Contain    ${t7}     750 ms

Validate default NV Setting - LRS numslots.n1
    ${n1}    Get Command Value    nv-get LRS    numslots.n1
    Log To Console    ${n1}
    Should Contain    ${n1}     16

Validate default NV Setting - LRS numslots.n2
    ${n2}    Get Command Value    nv-get LRS    numslots.n2
    Log To Console    ${n2}
    Should Contain    ${n2}     16

Validate default NV Setting - LRS numslots.n3
    ${n3}    Get Command Value    nv-get LRS    numslots.n3
    Log To Console    ${n3}
    Should Contain    ${n3}     128

Validate default NV Setting - LRS txSlot
    ${txSlot}    Get Command Value    nv-get LRS    txSlot
    Log To Console    ${txSlot}
    Should Contain    ${txSlot}     is not set

Validate default NV Setting - LRS txChID
    ${txChID}    Get Command Value    nv-get LRS    txChID
    Log To Console    ${txChID}
    Should Contain    ${txChID}     is not set

Validate default NV Setting - LRS save
    ${save}    Get Command Value    nv-get LRS    save
    Log To Console    ${save}
    Should Contain    ${save}     Configuration is saved to NV

# Validate default NV Setting - LRS stdChList
#     ${stdChList}    Get Command Value    nv-get LRS    stdChList
#     Log To Console    ${stdChList}
#     Should Contain    ${stdChList}     is not set

Validate default NV Setting - IMU magCalibration
    ${magCalibration}    Get Command Value    nv-get IMU    magCalibration
    Log To Console    ${magCalibration}
    Should Contain    ${magCalibration}     [0,0,0 : 0,0,0]

Validate default NV Setting - IMU magDeclination
    ${magDeclination}    Get Command Value    nv-get IMU    magDeclination
    Log To Console    ${magDeclination}
    Should Contain    ${magDeclination}     10

Validate default NV Setting - SKH
    ${SKH}    Get Command Value    nv-get SKH    ${EMPTY}
    Log To Console    ${SKH}
    Should Contain    ${SKH}     Empty

Validate default NV Setting - TER verbosity
    ${verbosity}    Get Command Value    nv-get TER    verbosity
    Log To Console    ${verbosity}
    Should Contain    ${verbosity}     all

Validate default NV Setting - WFi AccessPoint
    ${AccessPoint}    Get Command Value    nv-get WFi    AccessPoint
    Log To Console    ${AccessPoint}
    Should Contain    ${AccessPoint}     eShepherdAP
    Should Contain    ${AccessPoint}     6@AgTech