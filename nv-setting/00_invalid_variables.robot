*** settings ***
Resource    ../resources/variables.robot
Resource    ../resources/keywords.robot
Library    NeckbandOperation.py
Suite Setup    Setup MetaData

*** test cases ***
Validate Removed NV Setting - BatteryLow_Volts
    ${BatteryLow_Volts}    Check Invalid Command    nv-get App AppConfig.BatteryLow_Volts
    Log To Console    ${BatteryLow_Volts}
    Should Contain   ${BatteryLow_Volts}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - ShockCurrentRange_mA
    ${ShockCurrentRange_mA}    Check Invalid Command    nv-get App AppConfig.ShockCurrentRange_mA
    Log To Console    ${ShockCurrentRange_mA}
    Should Contain   ${ShockCurrentRange_mA}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - MaxTemp_DegreesC
    ${MaxTemp_DegreesC}    Check Invalid Command    nv-get App AppConfig.MaxTemp_DegreesC
    Log To Console    ${MaxTemp_DegreesC}
    Should Contain   ${MaxTemp_DegreesC}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - OperationMode
    ${OperationMode}    Check Invalid Command    nv-get App AppConfig.OperationMode
    Log To Console    ${OperationMode}
    Should Contain   ${OperationMode}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - GPS FixInterval
    ${FixInterval}    Check Invalid Command    nv-get App AppConfig.GPS.FixInterval
    Log To Console    ${FixInterval}
    Should Contain   ${FixInterval}    ${INVALID_VAR_NAME}
    # Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - GPS MaxMissedFixes
    ${MaxMissedFixes}    Check Invalid Command    nv-get App AppConfig.GPS.MaxMissedFixes
    Log To Console    ${MaxMissedFixes}
    Should Contain   ${MaxMissedFixes}    ${INVALID_VAR_NAME}
    # Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - GPS WaitForLockDuration_sec
    ${WaitForLockDuration_sec}    Check Invalid Command    nv-get App AppConfig.GPS.WaitForLockDuration_sec
    Log To Console    ${WaitForLockDuration_sec}
    Should Contain   ${WaitForLockDuration_sec}    ${INVALID_VAR_NAME}
    # Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - GPS WaitForLockExpiry_sec
    ${WaitForLockExpiry_sec}    Check Invalid Command    nv-get App AppConfig.GPS.WaitForLockExpiry_sec
    Log To Console    ${WaitForLockExpiry_sec}
    Should Contain   ${WaitForLockExpiry_sec}    ${INVALID_VAR_NAME}
    # Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - NoLoRaCommsTimeout_mins
    ${NoLoRaCommsTimeout_mins}    Check Invalid Command    nv-get App AppConfig.NoLoRaCommsTimeout_mins
    Log To Console    ${NoLoRaCommsTimeout_mins}
    Should Contain   ${NoLoRaCommsTimeout_mins}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - PowerZone
    ${PowerZone}    Check Invalid Command    nv-get App AppConfig.PowerZone
    Log To Console    ${PowerZone}
    Should Contain   ${PowerZone}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - exclusionDistanceOffset
    ${exclusionDistanceOffset}    Check Invalid Command    nv-get App AppConfig.exclusionDistanceOffset
    Log To Console    ${exclusionDistanceOffset}
    Should Contain   ${exclusionDistanceOffset}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - sleepInPowerZoneTime_s
    ${sleepInPowerZoneTime_s}    Check Invalid Command    nv-get App AppConfig.sleepInPowerZoneTime_s
    Log To Console    ${sleepInPowerZoneTime_s}
    Should Contain   ${sleepInPowerZoneTime_s}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - sleepInPowerZone
    ${sleepInPowerZone}    Check Invalid Command    nv-get App AppConfig.sleepInPowerZone
    Log To Console    ${sleepInPowerZone}
    Should Contain   ${sleepInPowerZone}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - wifiOn
    ${wifiOn}    Check Invalid Command    nv-get App AppConfig.wifiOn
    Log To Console    ${wifiOn}
    Should Contain   ${wifiOn}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - hibernate
    ${hibernate}    Check Invalid Command    nv-get App AppConfig.hibernate
    Log To Console    ${hibernate}
    Should Contain   ${hibernate}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - loraHeartBeatTimeOutMin
    ${loraHeartBeatTimeOutMin}    Check Invalid Command    nv-get App AppConfig.loraHeartBeatTimeOutMin
    Log To Console    ${loraHeartBeatTimeOutMin}
    Should Contain   ${loraHeartBeatTimeOutMin}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - SpeedRange
    ${SpeedRange}    Check Invalid Command    nv-get App StimulusAlgorithm.SpeedRange
    Log To Console    ${SpeedRange}
    Should Contain   ${SpeedRange}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - audioFreq
    ${audioFreq}    Check Invalid Command    nv-get App StimulusAlgorithm.audioFreq
    Log To Console    ${audioFreq}
    Should Contain   ${audioFreq}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - AudioLevel
    ${AudioLevel}    Check Invalid Command    nv-get App StimulusAlgorithm.AudioLevel
    Log To Console    ${AudioLevel}
    Should Contain   ${AudioLevel}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - audioDuration[short]
    ${audioDurationshort}    Check Invalid Command    nv-get App StimulusAlgorithm.audioDuration[short]
    Log To Console    ${audioDurationshort}
    Should Contain   ${audioDurationshort}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - audioDuration[normal]
    ${audioDurationnormal}    Check Invalid Command    nv-get App StimulusAlgorithm.audioDuration[normal]
    Log To Console    ${audioDurationnormal}
    Should Contain   ${audioDurationnormal}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - delay[PostPanic]
    ${delayPostPanic}    Check Invalid Command    nv-get App StimulusAlgorithm.delay[PostPanic]
    Log To Console    ${delayPostPanic}
    Should Contain   ${delayPostPanic}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - delay[PostAudio]
    ${delayPostAudio}    Check Invalid Command    nv-get App StimulusAlgorithm.delay[PostAudio]
    Log To Console    ${delayPostAudio}
    Should Contain   ${delayPostAudio}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - delay[PostShock]
    ${delayPostShock}    Check Invalid Command    nv-get App StimulusAlgorithm.delay[PostShock]
    Log To Console    ${delayPostShock}
    Should Contain   ${delayPostShock}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - minDeltaDistance
    ${minDeltaDistance}    Check Invalid Command    nv-get App StimulusAlgorithm.minDeltaDistance
    Log To Console    ${minDeltaDistance}
    Should Contain   ${minDeltaDistance}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - maxDeltaHeading
    ${maxDeltaHeading}    Check Invalid Command    nv-get App StimulusAlgorithm.maxDeltaHeading
    Log To Console    ${maxDeltaHeading}
    Should Contain   ${maxDeltaHeading}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - useIMU
    ${useIMU}    Check Invalid Command    nv-get App StimulusAlgorithm.useIMU
    Log To Console    ${useIMU}
    Should Contain   ${useIMU}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - useLastSpeed
    ${useLastSpeed}    Check Invalid Command    nv-get App StimulusAlgorithm.useLastSpeed
    Log To Console    ${useLastSpeed}
    Should Contain   ${useLastSpeed}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - PWR batteryThrEmpty_mV
    ${batteryThrEmpty_mV}    Check Invalid Command    nv-get PWR batteryThrEmpty_mV
    Log To Console    ${batteryThrEmpty_mV}
    Should Contain   ${batteryThrEmpty_mV}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - PWR batteryThrFull_mV
    ${batteryThrFull_mV}    Check Invalid Command    nv-get PWR batteryThrFull_mV
    Log To Console    ${batteryThrFull_mV}
    Should Contain   ${batteryThrFull_mV}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - GPS minSpeedThresh
    # ${minSpeedThresh}    Check Invalid Command    nv-get GPS minSpeedThresh
    # Log To Console    ${minSpeedThresh}
    # Should Contain   ${minSpeedThresh}    ${INVALID_VAR_NAME}
    Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - GPS minDistanceThresh
    # ${minDistanceThresh}    Check Invalid Command    nv-get GPS minDistanceThresh
    # Log To Console    ${minDistanceThresh}
    # Should Contain   ${minDistanceThresh}    ${INVALID_VAR_NAME}
    Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - GPS gpsSpeedFilterEnabled
    # ${gpsSpeedFilterEnabled}    Check Invalid Command    nv-get GPS gpsSpeedFilterEnabled
    # Log To Console    ${gpsSpeedFilterEnabled}
    # Should Contain   ${gpsSpeedFilterEnabled}    ${INVALID_VAR_NAME}
    Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - GPS gpsIMUMovementFilterEnabled
    # ${gpsIMUMovementFilterEnabled}    Check Invalid Command    nv-get GPS gpsIMUMovementFilterEnabled
    # Log To Console    ${gpsIMUMovementFilterEnabled}
    # Should Contain   ${gpsIMUMovementFilterEnabled}    ${INVALID_VAR_NAME}
    Log    Removed GPS variables won't return anything.    WARN

Validate Removed NV Setting - LRS sequenceMax
    ${sequenceMax}    Check Invalid Command    nv-get LRS sequenceMax
    Log To Console    ${sequenceMax}
    Should Contain   ${sequenceMax}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - LRS timingt5
    ${timingt5}    Check Invalid Command    nv-get LRS timing.t5
    Log To Console    ${timingt5}
    Should Contain   ${timingt5}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - IMU MThreshold
    ${MThreshold}    Check Invalid Command    nv-get IMU MThreshold
    Log To Console    ${MThreshold}
    Should Contain   ${MThreshold}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - IMU CThreshold
    ${CThreshold}    Check Invalid Command    nv-get IMU CThreshold
    Log To Console    ${CThreshold}
    Should Contain   ${CThreshold}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - IMU GrazingPitch
    ${GrazingPitch}    Check Invalid Command    nv-get IMU GrazingPitch
    Log To Console    ${GrazingPitch}
    Should Contain   ${GrazingPitch}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - IMU MovingThreshold
    ${MovingThreshold}    Check Invalid Command    nv-get IMU MovingThreshold
    Log To Console    ${MovingThreshold}
    Should Contain   ${MovingThreshold}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - IMU SpeedFilterSize
    ${SpeedFilterSize}    Check Invalid Command    nv-get IMU SpeedFilterSize
    Log To Console    ${SpeedFilterSize}
    Should Contain   ${SpeedFilterSize}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - IMU slidingWindow
    ${slidingWindow}    Check Invalid Command    nv-get IMU slidingWindow
    Log To Console    ${slidingWindow}
    Should Contain   ${slidingWindow}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - SKD driveInductor_Intended_mA
    ${driveInductor_Intended_mA}    Check Invalid Command    nv-get IMU driveInductor_Intended_mA
    Log To Console    ${driveInductor_Intended_mA}
    Should Contain   ${driveInductor_Intended_mA}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - SKD pulseInterval_ms
    ${pulseInterval_ms}    Check Invalid Command    nv-get IMU pulseInterval_ms
    Log To Console    ${pulseInterval_ms}
    Should Contain   ${pulseInterval_ms}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - SKD numPulsesPerOnPeriod
    ${numPulsesPerOnPeriod}    Check Invalid Command    nv-get IMU numPulsesPerOnPeriod
    Log To Console    ${numPulsesPerOnPeriod}
    Should Contain   ${numPulsesPerOnPeriod}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - SKD numberOfPulseTrains
    ${numberOfPulseTrains}    Check Invalid Command    nv-get IMU numberOfPulseTrains
    Log To Console    ${numberOfPulseTrains}
    Should Contain   ${numberOfPulseTrains}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - SKD pulseTrainOffPeriod_ms
    ${pulseTrainOffPeriod_ms}    Check Invalid Command    nv-get IMU pulseTrainOffPeriod_ms
    Log To Console    ${pulseTrainOffPeriod_ms}
    Should Contain   ${pulseTrainOffPeriod_ms}    ${INVALID_VAR_NAME}

Validate Removed NV Setting - ACM triggerThresh
    # ${triggerThresh}    Check Invalid Command    nv-get ACM triggerThresh
    # Log To Console    ${triggerThresh}
    # Should Be True    ${triggerThresh}
    Log    Removed ACM variables won't return anything.    WARN

Validate Removed NV Setting - ACM duration
    # ${duration}    Check Invalid Command    nv-get ACM duration
    # Log To Console    ${duration}
    # Should Be True    ${duration}
    Log    Removed ACM variables won't return anything.    WARN

Validate Removed NV Setting - ACM sedentary
    # ${sedentary}    Check Invalid Command    nv-get ACM sedentary
    # Log To Console    ${sedentary}
    # Should Be True    ${sedentary}
    Log    Removed ACM variables won't return anything.    WARN

Validate Removed NV Setting - ACM active
    # ${active}    Check Invalid Command    nv-get ACM active
    # Log To Console    ${active}
    # Should Be True    ${active}
    Log    Removed ACM variables won't return anything.    WARN

Validate Removed NV Setting - ACM running
    # ${running}    Check Invalid Command    nv-get ACM running
    # Log To Console    ${running}
    # Should Be True    ${running}
    Log    Removed ACM variables won't return anything.    WARN

Validate Removed NV Setting - ACM error
    # ${error}    Check Invalid Command    nv-get ACM error
    # Log To Console    ${error}
    # Should Be True    ${error}
    Log    Removed ACM variables won't return anything.    WARN